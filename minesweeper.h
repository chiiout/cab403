#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>


# define MAXMINES 99
# define NUM_TILES_X 9
# define NUM_TILES_Y 9
# define NUM_MINES 10
# define RANDOM_NUMBER_SEED 42



bool isMine(int row, int col, char board[][NUM_TILES_Y]) {
  if (board[row][col] == '*')
    return true;
  else
    return false;
}


bool isValid(int row, int col) {
  return (row >= 0) && (row < NUM_TILES_X) &&
    (col >= 0) && (col < NUM_TILES_Y);
}



void printBoard(char myBoard[][NUM_TILES_Y], int connectfd) {
  send(connectfd, myBoard, 81, 0);
	send(connectfd, "1", 256, 0);
}


int countAdjacentMines(int row, int col, int mines[][2],
  char realBoard[][NUM_TILES_Y]) {
  int count = 0;

  if (isValid(row - 1, col) == true) {
    if (isMine(row - 1, col, realBoard) == true)
      count++;
  }
  if (isValid(row + 1, col) == true) {
    if (isMine(row + 1, col, realBoard) == true)
      count++;
  }
  if (isValid(row, col + 1) == true) {
    if (isMine(row, col + 1, realBoard) == true)
      count++;
  }
  if (isValid(row, col - 1) == true) {
    if (isMine(row, col - 1, realBoard) == true)
      count++;
  }
  if (isValid(row - 1, col + 1) == true) {
    if (isMine(row - 1, col + 1, realBoard) == true)
      count++;
  }
  if (isValid(row - 1, col - 1) == true) {
    if (isMine(row - 1, col - 1, realBoard) == true)
      count++;
  }
  if (isValid(row + 1, col + 1) == true) {
    if (isMine(row + 1, col + 1, realBoard) == true)
      count++;
  }
  if (isValid(row + 1, col - 1) == true) {
    if (isMine(row + 1, col - 1, realBoard) == true)
      count++;
  }
  return (count);
}


bool playMinesweeperUtil(char myBoard[][NUM_TILES_Y], char realBoard[][NUM_TILES_Y],
  int mines[][2], int row, int col, int * movesLeft, int connectfd) {

  if (myBoard[row][col] != ' ')
    return (false);
  if (realBoard[row][col] == '*') {
    myBoard[row][col] = '*';
    for (int i = 0; i < NUM_MINES; i++)
      myBoard[mines[i][0]][mines[i][1]] = '*';
		// Game over
    send(connectfd, myBoard, 81, 0);
		send(connectfd, "2", 256, 0);
		printf("\nGG\n");
    return (true);
  } else {
    int count = countAdjacentMines(row, col, mines, realBoard);
    ( * movesLeft) --;

    myBoard[row][col] = count + '0';

    if (!count) {
      if (isValid(row - 1, col) == true) {
        if (isMine(row - 1, col, realBoard) == false)
          playMinesweeperUtil(myBoard, realBoard, mines, row - 1, col, movesLeft, connectfd);
      }
      if (isValid(row + 1, col) == true) {
        if (isMine(row + 1, col, realBoard) == false)
          playMinesweeperUtil(myBoard, realBoard, mines, row + 1, col, movesLeft, connectfd);
      }
      if (isValid(row, col + 1) == true) {
        if (isMine(row, col + 1, realBoard) == false)
          playMinesweeperUtil(myBoard, realBoard, mines, row, col + 1, movesLeft, connectfd);
      }
      if (isValid(row, col - 1) == true) {
        if (isMine(row, col - 1, realBoard) == false)
          playMinesweeperUtil(myBoard, realBoard, mines, row, col - 1, movesLeft, connectfd);
      }
      if (isValid(row - 1, col + 1) == true) {
        if (isMine(row - 1, col + 1, realBoard) == false)
          playMinesweeperUtil(myBoard, realBoard, mines, row - 1, col + 1, movesLeft, connectfd);
      }
      if (isValid(row - 1, col - 1) == true) {
        if (isMine(row - 1, col - 1, realBoard) == false)
          playMinesweeperUtil(myBoard, realBoard, mines, row - 1, col - 1, movesLeft, connectfd);
      }
      if (isValid(row + 1, col + 1) == true) {
        if (isMine(row + 1, col + 1, realBoard) == false)
          playMinesweeperUtil(myBoard, realBoard, mines, row + 1, col + 1, movesLeft, connectfd);
      }
      if (isValid(row + 1, col - 1) == true) {
        if (isMine(row + 1, col - 1, realBoard) == false)
          playMinesweeperUtil(myBoard, realBoard, mines, row + 1, col - 1, movesLeft, connectfd);
      }
    }
    return false;
  }
}


void placeMines(int mines[][2], char realBoard[][NUM_TILES_Y]) {
  bool mark[NUM_TILES_X * NUM_TILES_Y];
  memset(mark, false, sizeof(mark));

  for (int i = 0; i < NUM_MINES;) {
    int random = rand() % (NUM_TILES_X * NUM_TILES_Y);
    int x = random / NUM_TILES_X;
    int y = random % NUM_TILES_Y;

    if (mark[random] == false) {
      mines[i][0] = x;
      mines[i][1] = y;

      realBoard[mines[i][0]][mines[i][1]] = '*';
      mark[random] = true;
      i++;
    }
  }
}


void initialise(char realBoard[][NUM_TILES_Y], char myBoard[][NUM_TILES_Y]) {
  srand(time(NULL));
  for (int i = 0; i < NUM_TILES_X; i++) {
    for (int j = 0; j < NUM_TILES_Y; j++) {
      myBoard[i][j] = realBoard[i][j] = ' ';
    }
  }
}

/*
void cheatMinesweeper(char realBoard[][NUM_TILES_Y]) {
  printf("The mines locations are-\n");
  printBoard(realBoard);
}
*/

void replaceMine(int row, int col, char board[][NUM_TILES_Y]) {
  for (int i = 0; i < NUM_TILES_X; i++) {
    for (int j = 0; j < NUM_TILES_Y; j++) {
      if (board[i][j] != '*') {
        board[i][j] = '*';
        board[row][col] = '-';
        return;
      }
    }
  }
}


void playMinesweeper(int connectfd) {
	srand(RANDOM_NUMBER_SEED);
  bool gameOver = false;
  char realBoard[NUM_TILES_X][NUM_TILES_Y], myBoard[NUM_TILES_X][NUM_TILES_Y];
  int movesLeft = NUM_TILES_X * NUM_TILES_Y - NUM_MINES, x, y, a, b;
  int mines[MAXMINES][2];
	
  initialise(realBoard, myBoard);
  placeMines(mines, realBoard);

  int currentMoveIndex = 0;
  while (gameOver == false) {
    printBoard(myBoard, connectfd);
    //makeMove( & x, & y);

		recv(connectfd, &a, sizeof(a), 0);
		recv(connectfd, &b, sizeof(b), 0);
		x = ntohl(a);
		y = ntohl(b);

    if (currentMoveIndex == 0) {
      if (isMine(x, y, realBoard) == true)
        replaceMine(x, y, realBoard);
    }
    currentMoveIndex++;
    gameOver = playMinesweeperUtil(myBoard, realBoard, mines, x, y, & movesLeft, connectfd);

    if ((gameOver == false) && (movesLeft == 0)) {
      printf("\nCongratulations, you have located all the mine\n");
			send(connectfd, myBoard, 81, 0);
			send(connectfd, "3", 256, 0);
      gameOver = true;
    }
  }
}
