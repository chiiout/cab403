#include <stdio.h> 
#include <string.h>
#include <stdlib.h>

void login(char username[10], char password[10], int connectFd) {
	char str[60], name[10], pwd[10];
	int flag = 0;

	FILE *fp =  fopen("Authentication.txt" , "r");
    if(fp == NULL) {
    	perror("Error opening file");
      exit(1);
    }

  while( fgets (str, 60, fp) ) {
   	sscanf(str,"%s %s", name, pwd);		
		if (strcmp(name, username) == 0 && strcmp(pwd, password) == 0) {
		 	printf("Credential Matched\n");
			flag = 1;
		}				
	}	    

	if (flag == 1){
		send(connectFd, "1", 256, 0);
		printf("User login successfully\n");
	}
	else if (flag == 0 ) {
		send(connectFd, "2", 256, 0);
		printf("Wrong credential entered, client disconnect\n");
	}
			
	fclose(fp);
}
