#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>


# define MAXMINES 99
# define NUM_TILES_X 9
# define NUM_TILES_Y 9
# define NUM_MINES 10

// Function to get the user's move 
void makeMove(int * x, int * y) {
  printf("Enter your move: (row, column):");
  scanf("%d %d", x, y);
  return;
}

/*Function to check whether given cell (row, col) 
 * has a mine or not. 
*/
bool isMine(int row, int col, char board[][NUM_TILES_Y]) {
  if (board[row][col] == '*')
    return true;
  else
    return false;
}

/*
 * Function to check whether given cell (row, col) 
 * is a valid cell or not 
*/
bool isValid(int row, int col) {
  /*
   *  Returns true if row number and column number 
   * is in range 
   */
  return (row >= 0) && (row < NUM_TILES_X) &&
    (col >= 0) && (col < NUM_TILES_Y);
}

// Function to print the current gameplay board 
void printBoard(char myBoard[][NUM_TILES_Y]) {
  printf(" ");
  for (int i = 0; i < NUM_TILES_X; i++)
    printf("%d ", i);
  printf("\n\n");
  for (int i = 0; i < NUM_TILES_Y; i++) {
    printf("%d ", i);
    for (int j = 0; j < NUM_TILES_X; j++)
      printf("%c ", myBoard[i][j]);
    printf("\n");
  }
  return;
}

// Function to count the number of 
// mines in the adjacent cells 
int countAdjacentMines(int row, int col, int mines[][2],
  char realBoard[][NUM_TILES_Y]) {
  int count = 0;

  /*
        Cell-->Current Cell (row, col) 
        N -->  North        (row-1, col) 
        S -->  South        (row+1, col) 
        E -->  East         (row, col+1) 
        W -->  West            (row, col-1) 
        N.E--> North-East   (row-1, col+1) 
        N.W--> North-West   (row-1, col-1) 
        S.E--> South-East   (row+1, col+1) 
        S.W--> South-West   (row+1, col-1) 
    */
  if (isValid(row - 1, col) == true) {
    if (isMine(row - 1, col, realBoard) == true)
      count++;
  }
  if (isValid(row + 1, col) == true) {
    if (isMine(row + 1, col, realBoard) == true)
      count++;
  }
  if (isValid(row, col + 1) == true) {
    if (isMine(row, col + 1, realBoard) == true)
      count++;
  }
  if (isValid(row, col - 1) == true) {
    if (isMine(row, col - 1, realBoard) == true)
      count++;
  }
  if (isValid(row - 1, col + 1) == true) {
    if (isMine(row - 1, col + 1, realBoard) == true)
      count++;
  }
  if (isValid(row - 1, col - 1) == true) {
    if (isMine(row - 1, col - 1, realBoard) == true)
      count++;
  }
  if (isValid(row + 1, col + 1) == true) {
    if (isMine(row + 1, col + 1, realBoard) == true)
      count++;
  }
  if (isValid(row + 1, col - 1) == true) {
    if (isMine(row + 1, col - 1, realBoard) == true)
      count++;
  }
  return (count);
}

// Recursive Fucntion to play the Minesweeper Game
bool playMinesweeperUtil(char myBoard[][NUM_TILES_Y], char realBoard[][NUM_TILES_Y],
  int mines[][2], int row, int col, int * movesLeft) {
  // Base Case of Recursion
  if (myBoard[row][col] != ' ')
    return (false);
  // You opened a mine, going to lose 
  if (realBoard[row][col] == '*') {
    myBoard[row][col] = '*';
    for (int i = 0; i < NUM_MINES; i++)
      myBoard[mines[i][0]][mines[i][1]] = '*';

    printBoard(myBoard);
    printf("\nGame over! You hit a mine.\n");
    return (true);
  } 
  /* Calculate the number of adjacent mines and put it 
   * on the board
   */ 
  else {
    int count = countAdjacentMines(row, col, mines, realBoard);
    ( * movesLeft) --;

    myBoard[row][col] = count + '0';
    // Recur for all 8 adjacent cells 
    if (!count) {
      if (isValid(row - 1, col) == true) {
        if (isMine(row - 1, col, realBoard) == false)
          playMinesweeperUtil(myBoard, realBoard, mines, row - 1, col, movesLeft);
      }
      if (isValid(row + 1, col) == true) {
        if (isMine(row + 1, col, realBoard) == false)
          playMinesweeperUtil(myBoard, realBoard, mines, row + 1, col, movesLeft);
      }
      if (isValid(row, col + 1) == true) {
        if (isMine(row, col + 1, realBoard) == false)
          playMinesweeperUtil(myBoard, realBoard, mines, row, col + 1, movesLeft);
      }
      if (isValid(row, col - 1) == true) {
        if (isMine(row, col - 1, realBoard) == false)
          playMinesweeperUtil(myBoard, realBoard, mines, row, col - 1, movesLeft);
      }
      if (isValid(row - 1, col + 1) == true) {
        if (isMine(row - 1, col + 1, realBoard) == false)
          playMinesweeperUtil(myBoard, realBoard, mines, row - 1, col + 1, movesLeft);
      }
      if (isValid(row - 1, col - 1) == true) {
        if (isMine(row - 1, col - 1, realBoard) == false)
          playMinesweeperUtil(myBoard, realBoard, mines, row - 1, col - 1, movesLeft);
      }
      if (isValid(row + 1, col + 1) == true) {
        if (isMine(row + 1, col + 1, realBoard) == false)
          playMinesweeperUtil(myBoard, realBoard, mines, row + 1, col + 1, movesLeft);
      }
      if (isValid(row + 1, col - 1) == true) {
        if (isMine(row + 1, col - 1, realBoard) == false)
          playMinesweeperUtil(myBoard, realBoard, mines, row + 1, col - 1, movesLeft);
      }
    }
    return false;
  }
}

/*
 * Function to place the mines randomly 
 * on the board
 */
void placeMines(int mines[][2], char realBoard[][NUM_TILES_Y]) {
  bool mark[NUM_TILES_X * NUM_TILES_Y];
  memset(mark, false, sizeof(mark));

  for (int i = 0; i < NUM_MINES;) {
    int random = rand() % (NUM_TILES_X * NUM_TILES_Y);
    int x = random / NUM_TILES_X;
    int y = random % NUM_TILES_Y;

    if (mark[random] == false) {
      mines[i][0] = x;
      mines[i][1] = y;

      realBoard[mines[i][0]][mines[i][1]] = '*';
      mark[random] = true;
      i++;
    }
  }
  return;
}

// Function to initialise the game 
void initialise(char realBoard[][NUM_TILES_Y], char myBoard[][NUM_TILES_Y]) {
   /* 
    * Initiate the random number generator so that 
    * the same configuration doesn't arises 
    */
  srand(time(NULL));
  for (int i = 0; i < NUM_TILES_X; i++) {
    for (int j = 0; j < NUM_TILES_Y; j++) {
      myBoard[i][j] = realBoard[i][j] = ' ';
    }
  }
  return;
}

/*
 * Function to replace the mine from (row, col) and put 
 * it to a vacant space 
 */
void replaceMine(int row, int col, char board[][NUM_TILES_Y]) {
  for (int i = 0; i < NUM_TILES_X; i++) {
    for (int j = 0; j < NUM_TILES_Y; j++) {
      if (board[i][j] != '*') {
        board[i][j] = '*';
        board[row][col] = '-';
        return;
      }
    }
  }
  return;
}

// Function to play Minesweeper game 
void playMinesweeper() {
  // Initially the game is not over
  bool gameOver = false;
  // Actual Board and My Board 
  char realBoard[NUM_TILES_X][NUM_TILES_Y], myBoard[NUM_TILES_X][NUM_TILES_Y];
  int movesLeft = NUM_TILES_X * NUM_TILES_Y - NUM_MINES, x, y;
  int mines[MAXMINES][2];
  initialise(realBoard, myBoard);
  // Place the Mines randomly 
  placeMines(mines, realBoard);

  // You are in the game until you have not opened a mine So keep playing 
  int currentMoveIndex = 0;
  while (gameOver == false) {
    printBoard(myBoard);
    makeMove( & x, & y);

    /* This is to guarantee that the first move is 
     * always safe 
     * If it is the first move of the game
     */ 
    if (currentMoveIndex == 0) {
      if (isMine(x, y, realBoard) == true)
        replaceMine(x, y, realBoard);
    }
    currentMoveIndex++;
    gameOver = playMinesweeperUtil(myBoard, realBoard, mines, x, y, & movesLeft);

    if ((gameOver == false) && (movesLeft == 0)) {
      printf("\nCongratulations, you have located all the mine");
      gameOver = true;
    }
  }
  return;
}


int main() {
  playMinesweeper();
  return 0;
}
