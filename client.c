#include <stdio.h> 
#include <stdlib.h> 
#include <time.h>
#include <errno.h> 
#include <netdb.h> 
#include <netinet/in.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <unistd.h>
#include <strings.h> 
#include <string.h> 
// function to print game board
void printBoard(char myBoard[][9]) {
  printf(" ");
  for (int i = 0; i < 9; i++){
    printf(" %d", i);
	}
  printf("\n--------------------\n");
  for (int i = 0; i < 9; i++) {
    printf("%d", i);
    for (int j = 0; j < 9; j++){
      printf(" %c", myBoard[i][j]);
		}
    printf("\n");
  }
}


int main(int argc, char *argv[])
{
  int sockFd, a, b, x, y;
  struct sockaddr_in serverAddr;
  struct hostent *he;
	clock_t tic, toc;

  char buffer[256], username[20], password[20], selection[20];
	char myboard[9][9];
  
  if (argc != 3) {
		fprintf(stderr,"usage: client_hostname port_number\n");
		exit(1);
	}
  
  if ((he=gethostbyname(argv[1])) == NULL) {  /* get the host info */
		fprintf(stderr,"hostname error, exit\n");
		exit(1);
	}
  
  if ((sockFd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}  
  
  bzero(&serverAddr, sizeof(serverAddr));
		
  serverAddr.sin_addr = *((struct in_addr *)he->h_addr_list[0]);
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(atoi(argv[2]));
  
  if (connect(sockFd, (struct sockaddr *)&serverAddr, sizeof(struct sockaddr)) == -1) {
		perror("connect");
		exit(1);
	}

	// program entry txt
	printf("=====================================================\n");
	printf("Welcome to the online Minesweeper gaming system!\n");
	printf("=====================================================\n");

	printf("You are required to log on with your registered username and password\n");

	bzero(username,20);
	bzero(password,20);
	bzero(selection,20);

	// get credential detail
	printf("Username: ");
	scanf("%s", username);
	printf("Password: ");
	scanf("%s",password);
		
	// send username
	if (send(sockFd,username, sizeof(username), 0) == -1) {
		perror("recv");
		exit(1);
	}	
	// send password
	if (send(sockFd,password, sizeof(password), 0) == -1) {
		perror("recv");
		exit(1);
	} 

	// receive info for login
	if (recv(sockFd, buffer, sizeof(buffer), 0) == -1) {
		perror("recv"); 
		exit(1);
	}
	// continue if credential are correct/ or quit
	if (atoi(buffer) == 1){
		printf("Login Success!\n");
	}	
	else if (atoi(buffer) == 2){
		printf("You entered either an incorrect username or password. Disconnecting\n");
		exit(1);
	}

	bzero(buffer,256);

	// menu text & get selection
	printf("Welcome to the Minesweeper Gaming system\n");
	printf("\nPlease enter a selection:\n");
	printf("<1> Play Minesweeper\n");
	printf("<2> Show Leaderboard(not implemented)\n");
	printf("<3> Quit\n");
	printf("\nSelect option (1-3):");
	scanf("%s",selection);

	if (atoi(selection) == 1 || atoi(selection) == 2){
		send(sockFd,selection, sizeof(selection), 0);
	} 
	else if (atoi(selection) == 3) {
		printf("Program exit\n");
		send(sockFd,selection, sizeof(selection), 0);
		exit(1);
	}
	bzero(selection, sizeof(selection));
	// playing game

	tic = clock();
	while (1){	
		recv(sockFd, myboard, 81, 0);
		recv(sockFd, buffer, 256, 0);	
		printBoard(myboard);	

		// monitor game state
		if (atoi(buffer) == 1 ){
		// game continue
		} 
		else if (atoi(buffer) == 2) {
			printf("\nGame over! You hit a mine.\n\n");
			break;
		}
		else if (atoi(buffer) == 3) {
			printf("\nGame over! You WIN.\n\n");
			break;
		}

		bzero(myboard, 81);
		bzero(buffer, 256);
		printf("\nEnter your move: (row, column):");
		scanf("%d %d", &a, &b);

		x = htonl(a);
		y = htonl(b);

		send(sockFd, &x, sizeof(x), 0);
		send(sockFd, &y, sizeof(y), 0);

		bzero(&x, sizeof(x));
		bzero(&y, sizeof(y));
	}	
	toc = clock();
	printf("Game Time Played: %f seconds\n", (double)(toc - tic) / 100 );

	
	close(sockFd);
	
  return 0;
}
