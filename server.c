#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <strings.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h> 
#include <unistd.h>
#include <errno.h>
#include "login.h"
#include "minesweeper.h"


int main(int argc, char *argv[])
{
  struct sockaddr_in serverAddr;
  struct sockaddr_in clientAddr;
	socklen_t clientsize;
  int sockfd, connectFd;
  int portNo = 12345;
	char username[20], password[20], selection[20];
  
  // custom port number
  if (argc > 1) {
		printf("using custom port_number\n");
    portNo = atoi(argv[1]);
    printf("Port number: %d\n", portNo);
	}
  
  // generate the socket 
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket error");
		exit(1);
	}
  
  bzero(&serverAddr, sizeof(serverAddr));
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  serverAddr.sin_port = htons(portNo);  
  
  /* bind the socket to the end point */
	if (bind(sockfd, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) == -1) {
		perror("bind error");
		exit(1);
	}
  
  /* start listnening */
	if (listen(sockfd, 10) == -1) {
		perror("listen error");
		exit(1);
  }

  printf("server starts listnening ...\n");
  
  
  while (1) {
		clientsize = sizeof(struct sockaddr_in);
		if ((connectFd = accept(sockfd, (struct sockaddr *)&clientAddr, &clientsize)) == -1) {
			perror("accept");
			continue;
		}
		printf("server: got connection from %s\n", inet_ntoa(clientAddr.sin_addr));  
		// recv username
		if (recv(connectFd, username, sizeof(username), 0) == -1) {
			perror("recv username");
			exit(1);
		}
		// recv password
		if (recv(connectFd, password, sizeof(password), 0) == -1) {
			perror("recv password");
			exit(1);
		}
		// user login
		login(username, password, connectFd);
		bzero(username, 20);
		bzero(password, 20);
		
		// get menu selection
		bzero(selection, 20);
		recv(connectFd, selection, sizeof(selection), 0);

		// do thing according to user selection
		if (atoi(selection) == 1){
			printf("play game\n");
			playMinesweeper(connectFd);
		}
		else if (atoi(selection) == 2) {
			printf("Leaderboard not implemented...\n");
		}
		else if (atoi(selection) == 3) {
			printf("Client disconnected\n");
		}
		bzero(selection, 20);		
  }

	close(connectFd);

  return 0;
}
